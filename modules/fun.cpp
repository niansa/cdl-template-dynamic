#include <vector>
#include <cdlpp/cdltypes.hpp>
using namespace std;
using namespace CDL;



class Fun {
    static void freenitro(CMessage, CChannel channel, cmdargs&) {
        channel->send("https://discordgift.site/qYdLLCnv7KYFsKVL");
    }

    static void afk(CMessage msg, CChannel channel, cmdargs&) {
        channel->send(msg->author->get_mention()+" is now AFK");
    }


public:
    Fun() {
        register_command("freenitro", freenitro, NO_ARGS);
        register_command("afk", afk, NO_ARGS);
    }

    ~Fun() {
        deregister_commands({"freenitro", "afk"});
    }
};


static Fun *instance;
extern "C" {
    void cdlpp_module_init() {
        instance = new Fun;
    }
    void cdlpp_module_deinit() {
        delete instance;
    }
}
