#include <vector>
#include <cdlpp/cdltypes.hpp>
#include "modman.hpp"
using namespace std;
using namespace CDL;



class Modman {
#define owneronly if (msg->author->id != env.settings["owner"]) return

    static void loadmod(CMessage msg, CChannel channel, cmdargs& args) {
        owneronly;
        try {
            auto occ = commands.size();
            load_module(args[0]);
            channel->send("Module loaded successfully. "+to_string(commands.size() - occ)+" new commands!");
        }  catch (exception&) {
            channel->send(":warning: Loading the module failed!");
        }
    }
    static void unloadmod(CMessage msg, CChannel channel, cmdargs& args) {
        owneronly;
        try {
            auto occ = commands.size();
            unload_module(args[0]);
            channel->send("Module unloaded successfully. "+to_string(occ - commands.size())+" commands removed!");
        }  catch (exception&) {
            channel->send(":warning: Unloading the module failed! Is the module unloadable?");
        }
    }
    static void reloadmod(CMessage msg, CChannel channel, cmdargs& args) {
        owneronly;
        unloadmod(msg, channel, args);
        loadmod(msg, channel, args);
    }


public:
    Modman() {
        register_command("loadmod", loadmod, 1);
        register_command("unloadmod", unloadmod, 1);
        register_command("reloadmod", reloadmod, 1);
    }
};


static Modman modman;
