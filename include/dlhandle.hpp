#include <string>
#include <exception>
#include <dlfcn.h>


class Dlhandle {
    void *chandle;

public:
    class Exception : public std::exception {
        std::string errmsg;
    public:
        Exception(std::string errmsg) {
            this->errmsg = errmsg;
        }
        virtual const char* what() const throw() {
            return errmsg.c_str();
        }
    };

    Dlhandle(std::string fpath, int flags = RTLD_LAZY) {
        chandle = dlopen(fpath.c_str(), flags);
        if (!chandle) {
            throw Exception("dlopen(): "+fpath);
        }
    }
    ~Dlhandle() {
        dlclose(chandle);
    }

    template<typename T>
    T* get(const std::string& fname) {
        dlerror(); // Clear error
        auto fres = reinterpret_cast<T*>(dlsym(chandle, fname.c_str()));
        return (dlerror()==NULL)?fres:nullptr;
    }
    auto get_fnc(const std::string& fname) {
        return get<void*(...)>(fname);
    }
};
