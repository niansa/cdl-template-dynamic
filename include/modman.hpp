#include <string>

void load_module(std::string filename);
void unload_module(std::string filename);
inline void reload_module(const std::string& filename) {
    unload_module(filename);
    load_module(filename);
}

void load_modules();
