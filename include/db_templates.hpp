#pragma once

namespace db_templates {
    enum type {
        member,
        guild,
        _end [[maybe_unused]], // Must be last one before _default
        _default = member // Must be last one
    };
}
