# CDL++ template (dynamic)
A CDL++ template for modular bots - dynamically loaded modules and stuff like that

## Requirements
As of now dynamically loading modules that way is only possible on POSIX.1-2001 compliant systems like Linux

## Creating config.json
Fill the config.json before trying to do anything;
you can add your own settings and access them via the `env.settings` object

## Installing CDL
To be able to build this project you need to install CDL++ first:

    git clone https://gitlab.com/niansa/cdlpp.git
    cd cdlpp
    mkdir build
    cd build
    cmake ..
    cmake .. -DCMAKE_BUILD_TYPE=Release # Change to "Debug" if you need debugging
    make -j$(nproc)
    sudo make install

## Building
    mkdir build
    cd build
    cmake ..
    make -j$(nproc)

## Adding modules
You may add dynamic modules to `/modules/CMakeLists.txt` by adding a call to `add_module` to the bottom. This template also ships with a non-dynamic module with commands to load/unload/reload modules at any time.

Non-dynamic modules can be added to `/CMakeLists.txt` to the call to `add_executable`.
