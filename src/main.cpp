#include <iostream>
#include <exception>
#include <cdlpp/cdltypes.hpp>
#include "db_templates.hpp"
#include "modman.hpp"
using namespace std;
using namespace CDL;



int main(int argc, char **argv) {
    load_modules();

    handlers::get_prefix = [] (CChannel, std::function<void (const string&)> cb) {
        cb("!");
    };

    using namespace intent_vals;
    main(argc, argv, GUILD_MESSAGES);
}

