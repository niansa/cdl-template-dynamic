#include <string>
#include <map>
#include <iostream>
#include <exception>
#include <filesystem>
#include "dlhandle.hpp"
#include "modman.hpp"

static std::map<std::string, Dlhandle*> dlhandles;


void load_module(std::string filename) {
    filename = std::filesystem::canonical(filename);
    // Do not load a module that is already loaded
    if (dlhandles.find(filename) != dlhandles.end()) {
        throw std::exception();
    }
    // Create handle
    auto module = new Dlhandle(filename, RTLD_GLOBAL | RTLD_LAZY);
    // Run initialisation
    auto fnc = module->get_fnc("cdlpp_module_init");
    if (not fnc) {
        throw std::exception();
    }
    fnc();
    // Add module to list of loaded modules
    dlhandles[filename] = module;
}
void unload_module(std::string filename) {
    filename = std::filesystem::weakly_canonical(filename);
    // Find loaded module
    auto module = dlhandles.find(filename);
    // Do not unload a module that is not loaded
    if (module == dlhandles.end()) {
        throw std::exception();
    }
    // Run deinitialisation
    auto fnc = module->second->get_fnc("cdlpp_module_deinit");
    if (not fnc) {
        throw std::exception();
    }
    fnc();
    // Delete handle
    delete module->second;
    dlhandles.erase(module);
}

void load_modules() {
    for (const auto& modulefile : std::filesystem::directory_iterator("modules")) {
        auto modulefilename = modulefile.path().generic_string();
        // Check if file might not be a module
        if (modulefilename.find("/lib") == std::string::npos) {
            continue;
        }
        // Try to load file as module
        std::clog << "Loading "+modulefilename+"..." << std::flush;
        try {
            load_module(modulefilename);
            std::clog << " Success" << std::endl;
        } catch (std::exception&) {
            std::clog << " Failed" << std::endl;
        }
    }
}
